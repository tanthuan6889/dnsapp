import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  Button,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  withSpring,
} from 'react-native-reanimated';

export default function Example() {
  const [bc, setBc] = React.useState('white');
  const [color, setColor] = React.useState('black');
  const [borderColor, setBorderColor] = React.useState('gray');
  const [today, setToday] = React.useState(false);
  const [buy, setBuy] = React.useState(false);
  const [sell, setSell] = React.useState(false);
  const [moon, setMoon] = React.useState(false);
  const [sun, setSun] = React.useState(false);
  const [day, setDay] = React.useState(false);
  const [month, setMonth] = React.useState(false);
  const [year, setYear] = React.useState(false);
  const [bc2, setBc2] = React.useState('white');

  const h1 = useSharedValue(5);
  const h2 = useSharedValue(5);
  const h3 = useSharedValue(5);
  const h4 = useSharedValue(5);
  const h5 = useSharedValue(5);
  const h6 = useSharedValue(5);
  const h7 = useSharedValue(5);

  const column1 = useAnimatedStyle(() => {
    return {
      height: withSpring(h1.value, {
        damping: 15,
      }),
    };
  });

  const column2 = useAnimatedStyle(() => {
    return {
      height: withSpring(h2.value, {
        damping: 15,
      }),
    };
  });

  const column3 = useAnimatedStyle(() => {
    return {
      height: withSpring(h3.value, {
        damping: 15,
      }),
    };
  });

  const column4 = useAnimatedStyle(() => {
    return {
      height: withSpring(h4.value, {
        damping: 15,
      }),
    };
  });

  const column5 = useAnimatedStyle(() => {
    return {
      height: withSpring(h5.value, {
        damping: 15,
      }),
    };
  });

  const column6 = useAnimatedStyle(() => {
    return {
      height: withSpring(h6.value, {
        damping: 15,
      }),
    };
  });

  const column7 = useAnimatedStyle(() => {
    return {
      height: withSpring(h7.value, {
        damping: 15,
      }),
    };
  });

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#F0F0F0'}}>
      <View style={{flex: 1, marginLeft: 15}}>
        <Text style={{color: 'gray', fontSize: 30}}>Total Balance</Text>
        <View style={{height: 10}} />
        <Text style={{color: 'black', fontSize: 35, fontWeight: '700'}}>
          $ 11250.0
        </Text>
      </View>
      <View style={{flex: 2.5, flexDirection: 'row'}}>
        <View style={{flex: 1, margin: 15, marginRight: 7.5}}>
          {moon === false && (
            <TouchableOpacity
              onPress={() => {
                setBc('#6169DC');
                setColor('white');
                setSun(true);
                setSell(true);
                setToday(true);
                setBuy(false);
                setDay(false);
                h1.value = 30;
                h2.value = 70;
                h3.value = 90;
                h4.value = 140;
                h5.value = 180;
                h6.value = 120;
                h7.value = 100;
              }}
              style={[styles.boxcpt, {backgroundColor: bc}]}>
              <View style={{flex: 1, marginLeft: 10}}>
                <Icon name="moon-o" size={25} color={color} />
              </View>
              <View style={{flex: 1}}>
                <Text style={{fontSize: 20, fontWeight: '700', color: color}}>
                  1.05 MON
                </Text>
              </View>
              <View style={{flex: 1}}>
                <View>
                  <Text style={{color: color}}>USD</Text>
                </View>
                <View style={{height: 5}} />
                <View style={{flexDirection: 'row'}}>
                  <Text style={{fontWeight: '700', color: color}}>$9521.1</Text>
                  <View style={{width: 15}} />
                  <Text style={{fontWeight: '700', color: '#16a085'}}>
                    +16.33%
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
          {moon === true && (
            <TouchableOpacity
              onPress={() => {
                setSun(true);
                setMoon(false);
                setBuy(false);
                setSell(true);
                setToday(true);
                setDay(false);
                setMonth(true);
                setYear(true);
                h1.value = 30;
                h2.value = 70;
                h3.value = 90;
                h4.value = 140;
                h5.value = 180;
                h6.value = 120;
                h7.value = 100;
              }}
              style={styles.boxcptinactive}>
              <View style={{flex: 1, marginLeft: 10}}>
                <Icon name="moon-o" size={25} color="black" />
              </View>
              <View style={{flex: 1}}>
                <Text style={{fontSize: 20, fontWeight: '700'}}>1.05 MON</Text>
              </View>
              <View style={{flex: 1}}>
                <View>
                  <Text style={{color: 'gray'}}>USD</Text>
                </View>
                <View style={{height: 5}} />
                <View style={{flexDirection: 'row'}}>
                  <Text style={{fontWeight: '700'}}>$9521.1</Text>
                  <View style={{width: 15}} />
                  <Text style={{fontWeight: '700', color: 'green'}}>
                    +16.33%
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        </View>
        <View style={{flex: 1, margin: 15, marginLeft: 7.5}}>
          {sun === false && (
            <TouchableOpacity
              onPress={() => {
                setBc('#6169DC');
                setColor('white');
                setMoon(true);
                setSell(true);
                setToday(true);
                setBuy(false);
                setDay(false);
                h1.value = 50;
                h2.value = 90;
                h3.value = 90;
                h4.value = 50;
                h5.value = 200;
                h6.value = 140;
                h7.value = 160;
              }}
              style={[styles.boxcpt, {backgroundColor: bc}]}>
              <View style={{flex: 1, marginLeft: 10}}>
                <Icon name="sun-o" size={25} color={color} />
              </View>
              <View style={{flex: 1}}>
                <Text style={{fontSize: 20, fontWeight: '700', color: color}}>
                  0.83 SUN
                </Text>
              </View>
              <View style={{flex: 1}}>
                <View>
                  <Text style={{color: color}}>USD</Text>
                </View>
                <View style={{height: 5}} />
                <View style={{flexDirection: 'row'}}>
                  <Text style={{fontWeight: '700', color: color}}>$2302.8</Text>
                  <View style={{width: 15}} />
                  <Text style={{fontWeight: '700', color: 'red'}}>-9.52%</Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
          {sun === true && (
            <TouchableOpacity
              onPress={() => {
                setMoon(true);
                setSun(false);
                setBuy(false);
                setSell(true);
                setToday(true);
                setDay(false);
                setMonth(true);
                setYear(true);
                h1.value = 50;
                h2.value = 90;
                h3.value = 90;
                h4.value = 50;
                h5.value = 200;
                h6.value = 140;
                h7.value = 160;
              }}
              style={styles.boxcptinactive}>
              <View style={{flex: 1, marginLeft: 10}}>
                <Icon name="sun-o" size={25} color="black" />
              </View>
              <View style={{flex: 1}}>
                <Text style={{fontSize: 20, fontWeight: '700'}}>0.83 SUN</Text>
              </View>
              <View style={{flex: 1}}>
                <View>
                  <Text style={{color: 'gray'}}>USD</Text>
                </View>
                <View style={{height: 5}} />
                <View style={{flexDirection: 'row'}}>
                  <Text style={{fontWeight: '700'}}>$2302.8</Text>
                  <View style={{width: 15}} />
                  <Text style={{fontWeight: '700', color: 'red'}}>-9.52%</Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
        </View>
      </View>
      <View style={{flex: 4, backgroundColor: 'white'}}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <View
            style={{
              flex: 2,
              alignItems: 'center',
              justifyContent: 'center',
              marginLeft: 15,
              marginRight: 50,
            }}>
            <View style={{flexDirection: 'row'}}>
              {/* BUY DEFAULT */}
              {buy === false && moon === false && sun === false && (
                <TouchableOpacity
                  onPress={() => {
                    setBc('#6169DC');
                    setColor('white');
                    setBorderColor('#6169DC');
                    setToday(true);
                    setSell(true);
                    setSun(true);
                    setMoon(false);
                    h1.value = 50;
                    h2.value = 90;
                    h3.value = 90;
                    h4.value = 50;
                    h5.value = 200;
                    h6.value = 140;
                    h7.value = 160;
                  }}
                  style={[
                    styles.button,
                    {backgroundColor: bc, borderColor: borderColor},
                  ]}>
                  <Text style={[styles.textactive, {color: color}]}>Buy</Text>
                </TouchableOpacity>
              )}

              {/* BUY WITH SUN */}
              {buy === false && sun === false && moon === true && (
                <TouchableOpacity
                  onPress={() => {
                    setToday(true);
                    setSell(true);
                    setSun(false);
                    setMoon(true);
                    h1.value = 50;
                    h2.value = 90;
                    h3.value = 90;
                    h4.value = 50;
                    h5.value = 200;
                    h6.value = 140;
                    h7.value = 160;
                  }}
                  style={styles.buttonactive}>
                  <Text style={styles.textactive}>Buy</Text>
                </TouchableOpacity>
              )}

              {/* BUY WITH MON */}
              {buy === false && moon === false && sun === true && (
                <TouchableOpacity
                  onPress={() => {
                    setToday(true);
                    setSell(true);
                    setSun(true);
                    setMoon(false);
                    h1.value = 30;
                    h2.value = 70;
                    h3.value = 90;
                    h4.value = 140;
                    h5.value = 180;
                    h6.value = 120;
                    h7.value = 100;
                  }}
                  style={styles.buttonactive}>
                  <Text style={styles.textactive}>Buy</Text>
                </TouchableOpacity>
              )}

              {/* BUY RETURN  WITH SUN*/}
              {buy === true && moon === true && sun === false && (
                <TouchableOpacity
                  onPress={() => {
                    setToday(true);
                    setSell(true);
                    setBuy(false);
                    h1.value = 50;
                    h2.value = 90;
                    h3.value = 90;
                    h4.value = 50;
                    h5.value = 200;
                    h6.value = 140;
                    h7.value = 160;
                  }}
                  style={styles.buttoninactive}>
                  <Text style={styles.textinactive}>Buy</Text>
                </TouchableOpacity>
              )}

              {/* BUY RETURN  WITH MOON*/}
              {buy === true && moon === false && sun === true && (
                <TouchableOpacity
                  onPress={() => {
                    setToday(true);
                    setSell(true);
                    setBuy(false);
                    h1.value = 30;
                    h2.value = 70;
                    h3.value = 90;
                    h4.value = 140;
                    h5.value = 180;
                    h6.value = 120;
                    h7.value = 100;
                  }}
                  style={styles.buttoninactive}>
                  <Text style={styles.textinactive}>Buy</Text>
                </TouchableOpacity>
              )}

              {/* SELL DEFAULT */}
              {sell === false && moon === false && sun === false && (
                <TouchableOpacity
                  onPress={() => {
                    setBc('#6169DC');
                    setColor('white');
                    setBorderColor('#6169DC');
                    setBuy(true);
                    setToday(true);
                    setSun(true);
                    setMoon(false);
                    h1.value = 50;
                    h2.value = 60;
                    h3.value = 90;
                    h4.value = 150;
                    h5.value = 120;
                    h6.value = 170;
                    h7.value = 200;
                  }}
                  style={[
                    styles.button,
                    {backgroundColor: bc, borderColor: borderColor},
                  ]}>
                  <Text style={[styles.textactive, {color: color}]}>Sell</Text>
                </TouchableOpacity>
              )}

              {/* SELL WITH MOON */}
              {sell === false && moon === false && sun === true && (
                <TouchableOpacity
                  onPress={() => {
                    setBuy(true);
                    setToday(true);
                    setSun(true);
                    setMoon(false);
                    h1.value = 50;
                    h2.value = 60;
                    h3.value = 90;
                    h4.value = 150;
                    h5.value = 120;
                    h6.value = 170;
                    h7.value = 200;
                  }}
                  style={styles.buttonactive}>
                  <Text style={styles.textactive}>Sell</Text>
                </TouchableOpacity>
              )}

              {/* SELL WITH SUN */}
              {sell === false && moon === true && sun === false && (
                <TouchableOpacity
                  onPress={() => {
                    setBuy(true);
                    setToday(true);
                    setSun(false);
                    setMoon(true);
                    h1.value = 100;
                    h2.value = 120;
                    h3.value = 90;
                    h4.value = 150;
                    h5.value = 135;
                    h6.value = 180;
                    h7.value = 160;
                  }}
                  style={styles.buttonactive}>
                  <Text style={styles.textactive}>Sell</Text>
                </TouchableOpacity>
              )}

              {/* SELL RETURN WITH MOON */}
              {sell === true && moon === false && sun === true && (
                <TouchableOpacity
                  onPress={() => {
                    setToday(true);
                    setBuy(true);
                    setSell(false);
                    h1.value = 50;
                    h2.value = 60;
                    h3.value = 90;
                    h4.value = 150;
                    h5.value = 120;
                    h6.value = 170;
                    h7.value = 200;
                  }}
                  style={styles.buttoninactive}>
                  <Text style={styles.textinactive}>Sell</Text>
                </TouchableOpacity>
              )}

              {/* SELL RETURN WITH SUN */}
              {sell === true && moon === true && sun === false && (
                <TouchableOpacity
                  onPress={() => {
                    setToday(true);
                    setBuy(true);
                    setSell(false);
                    h1.value = 100;
                    h2.value = 120;
                    h3.value = 90;
                    h4.value = 150;
                    h5.value = 135;
                    h6.value = 180;
                    h7.value = 160;
                  }}
                  style={styles.buttoninactive}>
                  <Text style={styles.textinactive}>Sell</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>

          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              marginRight: 25,
            }}>
            {/* TODAY DEFAULT */}
            {today === false && moon === false && sun === false && (
              <TouchableOpacity
                onPress={() => {
                  setBc('#6169DC');
                  setColor('white');
                  setBorderColor('#6169DC');
                  setBuy(true);
                  setSell(true);
                  setSun(true);
                  setMoon(false);
                  setDay(true);
                  h1.value = 10;
                  h2.value = 20;
                  h3.value = 50;
                  h4.value = 90;
                  h5.value = 45;
                  h6.value = 55;
                  h7.value = 60;
                }}
                style={[
                  styles.buttontoday,
                  {backgroundColor: bc, borderColor: borderColor},
                ]}>
                <Text style={[styles.textactive, {color: color}]}>Today</Text>
              </TouchableOpacity>
            )}
            {/* TODAY WITH MOON */}
            {today === false && moon === false && sun === true && (
              <TouchableOpacity
                onPress={() => {
                  setBuy(true);
                  setSell(true);
                  setSun(true);
                  setMoon(false);
                  setDay(true);
                  h1.value = 10;
                  h2.value = 20;
                  h3.value = 50;
                  h4.value = 90;
                  h5.value = 45;
                  h6.value = 55;
                  h7.value = 60;
                }}
                style={styles.bttoday}>
                <Text style={styles.textactive}>Today</Text>
              </TouchableOpacity>
            )}

            {/* TODAY WITH SUN */}
            {today === false && moon === true && sun === false && (
              <TouchableOpacity
                onPress={() => {
                  setBuy(true);
                  setSell(true);
                  setSun(false);
                  setMoon(true);
                  setDay(true);
                  h1.value = 15;
                  h2.value = 19;
                  h3.value = 24;
                  h4.value = 76;
                  h5.value = 43;
                  h6.value = 56;
                  h7.value = 68;
                }}
                style={styles.bttoday}>
                <Text style={styles.textactive}>Today</Text>
              </TouchableOpacity>
            )}

            {/* TODAY RETURN WITH MOON */}
            {today === true && moon === false && sun === true && (
              <TouchableOpacity
                onPress={() => {
                  setBuy(true);
                  setSell(true);
                  setToday(false);
                  setDay(true);
                  h1.value = 10;
                  h2.value = 20;
                  h3.value = 50;
                  h4.value = 90;
                  h5.value = 45;
                  h6.value = 55;
                  h7.value = 60;
                }}
                style={styles.todayreturn}>
                <Text style={styles.textinactive}>Today</Text>
              </TouchableOpacity>
            )}

            {/* TODAY RETURN WITH SUN */}
            {today === true && moon === true && sun === false && (
              <TouchableOpacity
                onPress={() => {
                  setBuy(true);
                  setSell(true);
                  setToday(false);
                  setDay(true);
                  h1.value = 15;
                  h2.value = 19;
                  h3.value = 24;
                  h4.value = 76;
                  h5.value = 43;
                  h6.value = 56;
                  h7.value = 68;
                }}
                style={styles.todayreturn}>
                <Text style={styles.textinactive}>Today</Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <View style={{flex: 1, marginLeft: 15}}>
            <Text style={{color: 'gray', fontWeight: '700', fontSize: 20}}>
              Projection
            </Text>
            <View style={{height: 10}} />
            {moon === false && sun === true && (
              <View style={{flexDirection: 'row'}}>
                <Text style={{fontWeight: '700', fontSize: 16}}>$ 3010.18</Text>
                <View style={{width: 5}} />
                <Text style={{fontWeight: '700', fontSize: 16, color: 'green'}}>
                  +5.23%
                </Text>
              </View>
            )}

            {sun === false && moon === true && (
              <View style={{flexDirection: 'row'}}>
                <Text style={{fontWeight: '700', fontSize: 16}}>$ 2956.23</Text>
                <View style={{width: 5}} />
                <Text style={{fontWeight: '700', fontSize: 16, color: 'red'}}>
                  -7.83%
                </Text>
              </View>
            )}
          </View>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {/* DAY DEFAULT */}
            {day === false && moon === false && sun === false && (
              <TouchableOpacity
                onPress={() => {
                  setBc2('#DFDFF9');
                  setMonth(true);
                  setYear(true);
                }}
                style={[styles.section, {backgroundColor: bc2}]}>
                <Text style={{fontWeight: '700'}}>D</Text>
              </TouchableOpacity>
            )}

            {/* DAY WITH MOON BUY */}
            {day === false &&
              moon === false &&
              sun === true &&
              buy === false &&
              today === true && (
                <TouchableOpacity
                  onPress={() => {
                    setMonth(true);
                    setYear(true);
                    setMoon(false);
                    setSun(true);
                  }}
                  style={styles.section}>
                  <Text style={{fontWeight: '700'}}>D</Text>
                </TouchableOpacity>
              )}

            {/* DAY WITH SUN BUY */}
            {day === false &&
              moon === true &&
              sun === false &&
              buy === false &&
              today === true && (
                <TouchableOpacity
                  onPress={() => {
                    setMonth(true);
                    setYear(true);
                    setMoon(true);
                    setSun(false);
                  }}
                  style={styles.section}>
                  <Text style={{fontWeight: '700'}}>D</Text>
                </TouchableOpacity>
              )}

            {/* DAY RETURN WITH MOON BUY */}
            {day === true &&
              moon === false &&
              sun === true &&
              buy === false &&
              today === true && (
                <TouchableOpacity
                  onPress={() => {
                    setMonth(true);
                    setYear(true);
                    setMoon(false);
                    setSun(true);
                  }}
                  style={styles.section}>
                  <Text style={{fontWeight: '700'}}>D</Text>
                </TouchableOpacity>
              )}

            {/* DAY RETURN WITH SUN BUY */}
            {day === true &&
              moon === true &&
              sun === false &&
              buy === false &&
              today === true && (
                <TouchableOpacity
                  onPress={() => {
                    setMonth(true);
                    setYear(true);
                    setMoon(true);
                    setSun(false);
                  }}
                  style={styles.section}>
                  <Text style={{fontWeight: '700'}}>D</Text>
                </TouchableOpacity>
              )}

            {/* DAY WITH MOON SELL */}
            {day === false &&
              moon === false &&
              sun === true &&
              sell === false &&
              today === true && (
                <TouchableOpacity
                  onPress={() => {
                    setMonth(true);
                    setYear(true);
                    setMoon(false);
                    setSun(true);
                  }}
                  style={styles.section}>
                  <Text style={{fontWeight: '700'}}>D</Text>
                </TouchableOpacity>
              )}

            {/* DAY WITH SUN SELL */}
            {day === false &&
              moon === true &&
              sun === false &&
              sell === false &&
              today === true && (
                <TouchableOpacity
                  onPress={() => {
                    setMonth(true);
                    setYear(true);
                    setMoon(true);
                    setSun(false);
                  }}
                  style={styles.section}>
                  <Text style={{fontWeight: '700'}}>D</Text>
                </TouchableOpacity>
              )}

            {/* DAY RETURN WITH MOON SELL */}
            {day === true &&
              moon === false &&
              sun === true &&
              sell === false &&
              today === true && (
                <TouchableOpacity
                  onPress={() => {
                    setMonth(true);
                    setYear(true);
                    setMoon(false);
                    setSun(true);
                  }}
                  style={styles.section}>
                  <Text style={{fontWeight: '700'}}>D</Text>
                </TouchableOpacity>
              )}

            {/* DAY RETURN WITH SUN SELL */}
            {day === true &&
              moon === true &&
              sun === false &&
              sell === false &&
              today === true && (
                <TouchableOpacity
                  onPress={() => {
                    setMonth(true);
                    setYear(true);
                    setMoon(true);
                    setSun(false);
                  }}
                  style={styles.section}>
                  <Text style={{fontWeight: '700'}}>D</Text>
                </TouchableOpacity>
              )}

            {/* DAY DISABLE WHEN TODAY BUTTON ACTIVE */}
            {day === true &&
              today === false &&
              (moon === false || sun === false) && (
                <TouchableOpacity style={styles.sectioninactive}>
                  <Text style={{fontWeight: '700'}}>D</Text>
                </TouchableOpacity>
              )}

            <View style={{width: 15}} />

            {/* MONTH DEFAULT */}
            {month === false && (
              <TouchableOpacity
                onPress={() => {
                  setBc2('#DFDFF9');
                  setDay(true);
                  setYear(true);
                }}
                style={[styles.section, {backgroundColor: bc2}]}>
                <Text style={{fontWeight: '700'}}>M</Text>
              </TouchableOpacity>
            )}

            {month === true && (
              <TouchableOpacity
                onPress={() => {
                  setYear(true);
                  setDay(true);
                  setMonth(false);
                }}
                style={styles.sectioninactive}>
                <Text style={{fontWeight: '700'}}>M</Text>
              </TouchableOpacity>
            )}

            {/* MONTH WITH MOON BUY */}
            {/* {month === false && moon === false && sun === true && today === true && day === false &&
              (
                <TouchableOpacity
                  onPress={() => {
                    setDay(true);
                    setYear(true);
                    setMoon(false);
                    setSun(true);
                    h1.value = 80;
                    h2.value = 100;
                    h3.value = 120;
                    h4.value = 140;
                    h5.value = 160;
                    h6.value = 200;
                    h7.value = 180;
                  }}
                  style={{
                    height: 40,
                    width: 40,
                    backgroundColor: '#DFDFF9',
                    borderRadius: 8,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={{fontWeight: '700'}}>M</Text>
                </TouchableOpacity>
              )} */}

            {/* MONTH WITH SUN BUY */}
            {/* {month === false &&
              moon === true &&
              sun === false &&
              buy === false &&
              today === true && (
                <TouchableOpacity
                  onPress={() => {
                    setDay(true);
                    setYear(true);
                    setMoon(false);
                    setSun(true);
                    h1.value = 60;
                    h2.value = 70;
                    h3.value = 90;
                    h4.value = 120;
                    h5.value = 125;
                    h6.value = 140;
                    h7.value = 150;
                  }}
                  style={{
                    height: 40,
                    width: 40,
                    backgroundColor: '#DFDFF9',
                    borderRadius: 8,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={{fontWeight: '700'}}>M</Text>
                </TouchableOpacity>
              )} */}

            <View style={{width: 15}} />

            {year === false && (
              <TouchableOpacity
                onPress={() => {
                  setBc2('#DFDFF9');
                  setDay(true);
                  setMonth(true);
                }}
                style={[styles.section, {backgroundColor: bc2}]}>
                <Text style={{fontWeight: '700'}}>Y</Text>
              </TouchableOpacity>
            )}

            {year === true && (
              <TouchableOpacity
                onPress={() => {
                  setYear(false);
                  setDay(true);
                  setMonth(true);
                }}
                style={styles.sectioninactive}>
                <Text style={{fontWeight: '700'}}>Y</Text>
              </TouchableOpacity>
            )}
          </View>
        </View>
        <View style={{flex: 3}}>
          <View
            style={{
              flex: 6,
              marginHorizontal: 5,
              justifyContent: 'space-between',
              alignItems: 'flex-end',
              flexDirection: 'row',
            }}>
            <View style={{alignItems: 'center'}}>
              <Animated.View style={[styles.box1, column1]} />
              <Text>MON</Text>
            </View>

            <View style={{alignItems: 'center'}}>
              <Animated.View style={[styles.box2, column2]} />
              <Text>TUE</Text>
            </View>

            <View style={{alignItems: 'center'}}>
              <Animated.View style={[styles.box3, column3]} />
              <Text>WED</Text>
            </View>

            <View style={{alignItems: 'center'}}>
              <Animated.View style={[styles.box4, column4]} />
              <Text>THU</Text>
            </View>

            <View style={{alignItems: 'center'}}>
              <Animated.View style={[styles.box5, column5]} />
              <Text>FRI</Text>
            </View>

            <View style={{alignItems: 'center'}}>
              <Animated.View style={[styles.box6, column6]} />
              <Text>SAT</Text>
            </View>

            <View style={{alignItems: 'center'}}>
              <Animated.View style={[styles.box6, column7]} />
              <Text>SUN</Text>
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  box1: {
    width: 50,
    backgroundColor: '#9095e9',
    borderRadius: 8,
  },
  box2: {
    width: 50,
    backgroundColor: '#9095e9',
    borderRadius: 8,
  },
  box3: {
    width: 50,
    backgroundColor: '#9095e9',
    borderRadius: 8,
  },
  box4: {
    width: 50,
    backgroundColor: '#9095e9',
    borderRadius: 8,
  },
  box5: {
    width: 50,
    backgroundColor: '#9095e9',
    borderRadius: 8,
  },
  box6: {
    width: 50,
    backgroundColor: '#9095e9',
    borderRadius: 8,
  },
  box7: {
    width: 50,
    backgroundColor: '#9095e9',
    borderRadius: 8,
  },

  boxcpt: {
    flex: 1,
    borderRadius: 10,
    paddingLeft: 15,
    paddingTop: 15,
    shadowColor: '#000000',
    shadowOffset: {
      height: 0,
      width: 0,
    },
    shadowOpacity: 0.39,
    shadowRadius: 5,
    elevation: 1,
  },
  boxcptinactive: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: 10,
    paddingLeft: 15,
    paddingTop: 15,
  },
  button: {
    flex: 1,
    height: 60,
    width: 80,
    borderWidth: 1,
    borderRadius: 70,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonactive: {
    flex: 1,
    backgroundColor: '#6169DC',
    height: 60,
    width: 80,
    borderWidth: 1,
    borderRadius: 70,
    borderColor: '#6169DC',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttoninactive: {
    flex: 1,
    backgroundColor: 'white',
    height: 60,
    width: 80,
    borderWidth: 1,
    borderRadius: 70,
    borderColor: 'gray',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttontoday: {
    height: 60,
    width: 120,
    borderWidth: 1,
    borderRadius: 70,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bttoday: {
    backgroundColor: '#6169DC',
    height: 60,
    width: 120,
    borderWidth: 1,
    borderColor: '#6169DC',
    borderRadius: 70,
    alignItems: 'center',
    justifyContent: 'center',
  },
  todayreturn: {
    backgroundColor: 'white',
    height: 60,
    width: 120,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 70,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textactive: {
    fontWeight: '700',
    fontSize: 18,
    color: 'white',
  },
  textinactive: {
    fontWeight: '700',
    fontSize: 18,
    color: 'black',
  },
  section: {
    height: 40,
    width: 40,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DFDFF9',
  },
  sectioninactive: {
    height: 40,
    width: 40,
    backgroundColor: 'white',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
