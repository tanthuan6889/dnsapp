import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Button,
  Dimensions,
} from 'react-native';
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  withSpring,
  withTiming,
  Easing,
  withRepeat,
  withSequence,
  ANGLE,
} from 'react-native-reanimated';

const {width, height} = Dimensions.get('window');

export default function index() {
  const offset1 = useSharedValue(0);
  const offset2 = useSharedValue(0);
  const rotation1 = useSharedValue(0);
  const rotation2 = useSharedValue(0);

  const animatedStyleBox1 = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateY: withSpring(offset1.value, {
            damping: 20,
          }),
        },
        {rotateZ: withSpring(`${rotation1.value}deg`)},
      ],
    };
  });

  const animatedStyleBox2 = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateY: withSpring(offset2.value, {
            damping: 20,
          }),
        },
        {rotateZ: withSpring(`${rotation2.value}deg`)},
      ],
    };
  });

  return (
    <SafeAreaView style={{flex: 1, alignItems: 'center', marginTop: 150}}>
      <Animated.View style={[styles.box1, animatedStyleBox1]}>
        <View
          style={{marginLeft: 10, marginTop: 5, flexDirection: 'row', flex: 1}}>
          <Text style={{color: 'red', fontWeight: '700', fontSize: 30}}>
            TECHCOM
          </Text>
          <Text style={{color: 'black', fontWeight: '700', fontSize: 30}}>
            BANK
          </Text>
        </View>
        <View style={{alignItems: 'center', flex: 3, justifyContent: 'center'}}>
          <Text style={{fontWeight: '700', color: '#dfe6e9', fontSize: 35}}>
            4221 4986 6237 5392
          </Text>
        </View>
      </Animated.View>

      <Animated.View style={[animatedStyleBox2]}>
        <View
          style={{
            height: 220,
            width: 380,
            backgroundColor: '#FF7F2E',
            borderRadius: 12,
            marginTop: 10,
            shadowColor: '#000000',
            shadowOffset: {
              height: 0,
              width: 0,
            },
            shadowOpacity: 0.39,
            shadowRadius: 5,
            elevation: 1,
            marginTop: -180,
          }}>
          <View
            style={{
              marginLeft: 10,
              marginTop: 5,
              flexDirection: 'row',
              flex: 1,
            }}>
            <Text style={{color: 'white', fontWeight: '700', fontSize: 30}}>
              SHB
            </Text>
            <Text style={{color: 'white', fontWeight: '700', fontSize: 30}}>
              BANK
            </Text>
          </View>
          <View
            style={{alignItems: 'center', flex: 3, justifyContent: 'center'}}>
            <Text style={{fontWeight: '700', color: '#f1c40f', fontSize: 35}}>
              9704 4310 1791 3409
            </Text>
          </View>
        </View>
      </Animated.View>

      <View style={{flex: 1, justifyContent: 'flex-end'}}>
        <Text style={{fontSize: 30}}>Choose your credit card</Text>
      </View>

      <View style={{flex: 1, justifyContent: 'flex-end'}}>
        <Button
          title="Open"
          onPress={() => {
            rotation1.value = withSequence(
              withTiming(-15, {duration: 2000}),
              withTiming(-15, {duration: 2000}),
            );
            rotation2.value = withSequence(
              withTiming(5, {duration: 2000}),
              withTiming(5, {duration: 2000}),
            );
            offset1.value = withSequence(
              withTiming(-100, {duration: 500}),
              withTiming(-100, {duration: 500}),
            );

            offset2.value = withSequence(
              withTiming(90, {duration: 500}),
              withTiming(90, {duration: 500}),
            );
          }}
        />
        <Button
          title="Close"
          onPress={() => {
            rotation1.value = 0;
            rotation2.value = 0;
            offset1.value = withSequence(
              withTiming(0, {duration: 600}),
              withTiming(0, {duration: 600}),
            );

            offset2.value = withSequence(
              withTiming(0, {duration: 600}),
              withTiming(0, {duration: 600}),
            );
          }}
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  box1: {
    // marginTop: 200,
    height: 220,
    width: 380,
    backgroundColor: '#b2bec3',
    borderRadius: 12,
    shadowColor: '#000000',
    shadowOffset: {
      height: 0,
      width: 0,
    },
    shadowOpacity: 0.39,
    shadowRadius: 5,
    elevation: 1,
  },
  box2: {},
});
