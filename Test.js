import React from 'react';
import {View, Text, SafeAreaView, Image, Button, Alert} from 'react-native';
import axios from 'axios';

import BGIMG from '../assets/images/bgimg.jpg';

export default function Test() {
  // const imageView = () => {
  //   return (
  //     <View style={{flex: 1}}>
  //       <Image source={BGIMG} style={{height: '50%', width: '100%'}} />
  //     </View>
  //   );
  // };

  // function ImageView() {
  //   return (
  //     <View style={{flex: 1}}>
  //       <Image source={BGIMG} style={{height: '50%', width: '100%'}} />
  //     </View>
  //   );
  // }

  const [loading, setLoading] = React.useState(false);
  const [refresh, setRefresh] = React.useState(0);

  const apiWithAxios = () => {
    // before
    setLoading(true);
    // axios
    const url = 'https://jsonplaceholder.typicode.com/users';
    // const data = {
    //   email: this.state.email,
    //   password: this.state.password,
    // };

    axios
      .get(url)
      .then(response => {
        setLoading(false);
        console.log(response.data);
        if (response.data.length === 0) {
          Alert.alert('Thông báo', 'Lấy dữ liệu không thành công');
        } else {
          Alert.alert('Thông báo', 'Lấy dữ liệu thành công');
        }
      })
      .catch(error => {
        setLoading(false);
        console.log(error);
        Alert.alert('Thông báo', 'Có lỗi xảy ra');
      });
  };

  React.useEffect(() => {
    apiWithAxios();

    return () => {};
  }, [refresh]);

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 1, alignItems: 'center'}}>
        <Text>Test Axios</Text>
      </View>

      {/* {imageView()}
      <ImageView /> */}

      <View style={{flex: 1}}>
        <Button
          title="Refresh"
          onPress={() => {
            setRefresh(refresh + 1);
          }}
        />
      </View>
    </SafeAreaView>
  );
}
