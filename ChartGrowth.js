import React from 'react'
import { View, Text, StyleSheet, SafeAreaView, Button } from 'react-native';

import Animated, {
  useSharedValue,
  useAnimatedStyle,
  withSpring,
} from 'react-native-reanimated';


export default function index() {

  const h1 = useSharedValue(5);
  const h2 = useSharedValue(5);
  const h3 = useSharedValue(5);
  const h4 = useSharedValue(5);
  const h5 = useSharedValue(5);
  const h6 = useSharedValue(5);

  const column1 = useAnimatedStyle(() => {
    return {
      height: withSpring(h1.value, {
        damping: 15,
      }),
    };
  });

  const column2 = useAnimatedStyle(() => {
    return {
      height: withSpring(h2.value, {
        damping: 15,
      }),
    };
  });

  const column3 = useAnimatedStyle(() => {
    return {
      height: withSpring(h3.value, {
        damping: 15,
      }),
    };
  });

  const column4 = useAnimatedStyle(() => {
    return {
      height: withSpring(h4.value, {
        damping: 15,
      }),
    };
  });

  const column5 = useAnimatedStyle(() => {
    return {
      height: withSpring(h5.value, {
        damping: 15,
      }),
    };
  });

  const column6 = useAnimatedStyle(() => {
    return {
      height: withSpring(h6.value, {
        damping: 15,
      }),
    };
  });

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 3, marginHorizontal: 5, justifyContent:'space-between', alignItems: 'flex-end', flexDirection: 'row'}}>
        <View style={{alignItems: 'center'}}>
          <Animated.View style={[styles.box1, column1]} />
          <Text>2014</Text>
        </View>

        <View style={{alignItems: 'center'}}>
          <Animated.View style={[styles.box2, column2]} />
          <Text>2016</Text>
        </View>

        <View style={{alignItems: 'center'}}>
          <Animated.View style={[styles.box3, column3]} />
          <Text>2017</Text>
        </View>

        <View style={{alignItems: 'center'}}>
          <Animated.View style={[styles.box4, column4]} />
          <Text>2018</Text>
        </View>

        <View style={{alignItems: 'center'}}>
          <Animated.View style={[styles.box5, column5]} />
          <Text>2019</Text>
        </View>

        <View style={{alignItems: 'center'}}>
          <Animated.View style={[styles.box6, column6]} />
          <Text>2020</Text>
        </View>

      </View>
      <View style={{flex: 1, alignItems:'center'}}>
        <Text style={{fontSize:20, fontWeight:'700'}}>Chart Economic Growth</Text>
        <Button
          title="Press"
          onPress={() => {
            h1.value = 300;
            h2.value = 350;
            h3.value = 400;
            h4.value = 450;
            h5.value = 550;
            h6.value = 300;
          }}
        />
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  box1: {
    width: 50,
    backgroundColor: '#fdcb6e',
  },
  box2: {
    width: 50,
    backgroundColor: '#e17055',
  },
  box3: {
    width: 50,
    backgroundColor: '#0984e3',
  },
  box4: {
    width: 50,
    backgroundColor: '#00b894',
  },
  box5: {
    width: 50,
    backgroundColor: '#a29bfe',
  },
  box6: {
    width: 50,
    backgroundColor: '#636e72',
  },
});