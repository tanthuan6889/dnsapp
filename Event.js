import React from 'react';
import {View, Text, SafeAreaView, StyleSheet} from 'react-native';
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  withSpring,
  withTiming,
  withRepeat,
  withSequence,
  useAnimatedGestureHandler,
} from 'react-native-reanimated';

import {TapGestureHandler, PanGestureHandler} from 'react-native-gesture-handler';

export default function index() {
  const pressed = useSharedValue(false);
  const startingPosition = 150;
  const x = useSharedValue(startingPosition);
  const y = useSharedValue(startingPosition);

  // const eventHandler = useAnimatedGestureHandler({
  //   onStart: () => {
  //     pressed.value = true;
  //   },
  //   onEnd: () => {
  //     pressed.value = false;
  //   },
  // });

  // const uas = useAnimatedStyle(() => {
  //   return {
  //     backgroundColor: pressed.value ? '#fab1a0' : '#b2bec3',
  //     transform: [{scale: withSpring(pressed.value ? 2 : 1, {
  //       damping: 20,
  //     })}],
  //   };
  // });

  const eventHandler = useAnimatedGestureHandler({
    onStart: (event, ctx) => {
      pressed.value = true;
      ctx.startX = x.value;
      ctx.startY = y.value;
    },
    onActive: (event, ctx) => {
      x.value = ctx.startX + event.translationX;
      y.value = ctx.startY + event.translationY;
    },
    onEnd: (event, ctx) => {
      pressed.value = false;
      // x.value = withSpring(startingPosition, {
      //   damping:15,
      // });
      // y.value = withSpring(startingPosition, {
      //   damping: 15,
      // });
    },
  });

  const uas = useAnimatedStyle(() => {
    return {
      backgroundColor: pressed.value ? '#fab1a0' : '#b2bec3',
      transform: [{ translateX: x.value }, { translateY: y.value }],
    };
  });

 

  return (
    <SafeAreaView>
      <PanGestureHandler onGestureEvent={eventHandler}>
        <Animated.View style={[styles.ball, uas]} />
      </PanGestureHandler>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  ball: {
    height: 100,
    width: 100,
    borderRadius: 50,
    backgroundColor: '#b2bec3',
  },
});